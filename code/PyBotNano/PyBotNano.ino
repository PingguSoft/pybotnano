#include <Arduino.h>
#include <stdarg.h>
#include <math.h>
#include "common.h"
#include "config.h"
#include "utils.h"
#include "imath.h"
#include "RobotArm.h"
#include "CommInterface.h"

/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/
#define MICROSTEPPING           16      // 8 or 16 for 1/8 or 1/16 driver microstepping (default:16)
#define MAX_COMM                2
#define MAX_TRAJECTORY_POINTS   40

/*
*****************************************************************************************
* MACROS
*****************************************************************************************
*/

/*
( x ) = real command / status
  

            90=screen, (0 = real)
              |
              |
180 (90) -----+-----  0 (-90)
              |
              |
*/


/*
*****************************************************************************************
* VARIABLES
*****************************************************************************************
*/
static RobotArm m_robot;
static u16      m_wTrajTolerances[3];
static u8       m_ucTrajMax = 0;
static s8       m_ucTrajIdx = 0;
static struct param_dk m_dkTrajs[MAX_TRAJECTORY_POINTS];


/*
*****************************************************************************************
* HANDLER
*****************************************************************************************
*/
class CommandHandler : public ProtocolCallback {
    public:
        CommandHandler(CommInterface *comm) : m_pComm(comm) {
            m_wTrajTolerances[MOT_A1] = TOLERANCE_TRAJECTORY_M1;
            m_wTrajTolerances[MOT_A2] = TOLERANCE_TRAJECTORY_M2;
            m_wTrajTolerances[MOT_Z] = TOLERANCE_TRAJECTORY_M3;
            m_pwm = 1000;
        }
        
        ~CommandHandler() {
        }
        
        s8 onCommand(u8 cmd, u8 *pData, u8 size) {
            s8      ret = -1;
            float   x, y;
            float   a1, a2;
            union params *param = (union params*)pData;

            switch (cmd) {
                case MODE_HELLO:
                    m_robot.enable();
                    LOG(LOG_LOW, F("->MSG: JJAH: HELLO!\r\n"));
                    break;

                case MODE_LOG_MASK: {
                    u8 mask = *pData - '0';
                    LOG(LOG_LOW, F("->MSG: JJAL: %2x\r\n"), mask);
                    setLogMask(mask);
                }
                break;

                case MODE_TEST: {
                    LOG(LOG_LOW, F("pwm:%4d\r\n"), m_pwm);
                    m_robot.writeServo(SERVO_WRIST, m_pwm);
                    m_pwm += 10;
/*                    
                    LOG(LOG_LOW, F("->MSG: JJAZ\r\n"));
                    m_robot.setHeight(rand32() % 120);
                    m_robot.updateSpeedAcc();
                    m_robot.adjustTrajectorySpeed();
*/                    
#if 0
                    LOG(F("SPEED XY:%d SPEED Z:%d ACC XY:%d ACC Z:%d\r\n"),
                        param->spd_acc.spd_xy, param->spd_acc.spd_z, param->spd_acc.acc_xy, param->spd_acc.acc_z);

                    m_robot.configSpeed((s32)MAX_SPEED_M1 * (s32)(param->spd_acc.spd_xy / 100.0),
                        (s32)MAX_SPEED_M2 * (s32)(param->spd_acc.spd_xy / 100.0),
                        (s32)MAX_SPEED_M3 * (s32)(param->spd_acc.spd_z / 100.0));
                    m_robot.configAcceleration((s32)MAX_ACCEL_M1 * (s32)(param->spd_acc.acc_xy / 100.0),
                        (s32)MAX_ACCEL_M2 * (s32)(param->spd_acc.acc_xy / 100.0),
                        (s32)MAX_ACCEL_M3 * (s32)(param->spd_acc.acc_z / 100.0));

                    m_robot.updateSpeedAcc();
#endif
                }
                break;

                case MODE_DK:
                    LOG(LOG_MID, F("->MSG: JJAM:%d %d %d\r\n"), param->dk.axis1, param->dk.axis2, param->dk.axis3);
                    if (param->dk.axis1 != NODATA || param->dk.axis2 != NODATA) {
                        float a1 = (float)param->dk.axis1 / 100.0f;
                        float a2 = (float)param->dk.axis2 / 100.0f;
                        if (a1 >= 360) {
                            a1 = 360 - a1;
                        }
                        if (a2 >= 360) {
                            a2 = 360 - a2;
                        }
                        m_robot.setAxis(a1, a2);
                        m_robot.updateSpeedAcc();
                        m_robot.adjustTrajectorySpeed();
                    }
                    if (param->dk.axis3 != NODATA) {
                        m_robot.setHeight((float)param->dk.axis3 / 100.0f);
                    }
                    if (param->dk.servoWrist != NODATA) {
                        m_robot.moveServo(SERVO_WRIST, param->dk.servoWrist / 1000.0f);
                    }
                    if (param->dk.servoGripper != NODATA) {
                        m_robot.moveServo(SERVO_GRIPPER, param->dk.servoGripper / 1000.0f);
                    }
                    m_robot.enable();
                    break;

                case MODE_IK:
                    LOG(LOG_LOW, F("->MSG: JJAI:\r\n"));
                    x = param->ik.x / 10.0f;
                    y = param->ik.y / 10.0f;
                    m_robot.inverseKinematic(x, y, ROBOT_ARM1_LENGTH, ROBOT_ARM2_LENGTH, param->ik.elbow, &a1, &a2);
                    m_robot.setAxis(a1, a2);
                    if (param->ik.y != NODATA) {
                        m_robot.setHeight(param->ik.z / 100.0f);
                    }
                    if (param->ik.servoWrist != NODATA) {
                        m_robot.moveServo(SERVO_WRIST, param->dk.servoWrist / 1000.0f);
                    }
                    if (param->ik.servoGripper != NODATA) {
                        m_robot.moveServo(SERVO_GRIPPER, param->dk.servoGripper / 1000.0f);
                    }
                    m_robot.updateSpeedAcc();
                    m_robot.adjustTrajectorySpeed();
                    m_robot.enable();
                    break;

                case MODE_TRAJECTORY:
                    if (param->tj.index == 0) {
                        m_ucTrajMax = 0;
                        m_ucTrajIdx = -1;
                        memset(m_dkTrajs, 0, sizeof(m_dkTrajs));
                    }
                    if (param->tj.index >= MAX_TRAJECTORY_POINTS || param->tj.index < 0) {
                        param->tj.index = MAX_TRAJECTORY_POINTS - 1;
                        LOG(LOG_LOW, F("-->TR POINT OVERFLOW!\r\n"));
                    } else {
                        m_dkTrajs[param->tj.index].axis1 = param->tj.x;
                        m_dkTrajs[param->tj.index].axis2 = param->tj.y;
                        m_dkTrajs[param->tj.index].axis3 = param->tj.z;
                        m_dkTrajs[param->tj.index].servoWrist = param->tj.servoWrist;
                        m_dkTrajs[param->tj.index].servoGripper = param->tj.servoGripper;
                    }
                    if (param->tj.last == 1) {
                        m_ucTrajMax = param->tj.index;
                        m_ucTrajIdx = 0;
                        for (u8 i = 0; i < m_ucTrajMax; i++) {
                            LOG(LOG_LOW, F("T %2d :%5d %5d %5d\r\n"), i, m_dkTrajs[i].axis1, m_dkTrajs[i].axis2, m_dkTrajs[i].axis3);
                        }
                    }
                    m_robot.enable();
                    break;

                case MODE_SET:
                    m_wTrajTolerances[0] = (param->spd_acc.spd_tj / 10.0f) * M1_AXIS_STEPS_PER_UNIT;
                    m_wTrajTolerances[1] = (param->spd_acc.spd_tj / 10.0f) * M2_AXIS_STEPS_PER_UNIT;
                    m_wTrajTolerances[2] = (param->spd_acc.spd_tj / 10.0f) * M3_AXIS_STEPS_PER_UNIT;
                    LOG(LOG_LOW, F("->MSG: JJAS:  SPEED XY:%d SPEED Z:%d ACC XY:%d ACC Z:%d TRAJ S:%d %d %d %d\r\n"),
                        param->spd_acc.spd_xy, param->spd_acc.spd_z, param->spd_acc.acc_xy, param->spd_acc.acc_z,
                        param->spd_acc.spd_tj, m_wTrajTolerances[0], m_wTrajTolerances[1], m_wTrajTolerances[2]);
                    m_robot.configSpeed((s32)MAX_SPEED_M1 * (s32)(param->spd_acc.spd_xy / 100.0f),
                        (s32)MAX_SPEED_M2 * (s32)(param->spd_acc.spd_xy / 100.0f),
                        (s32)MAX_SPEED_M3 * (s32)(param->spd_acc.spd_z / 100.0f));
                    m_robot.configAcceleration((s32)MAX_ACCEL_M1 * (s32)(param->spd_acc.acc_xy / 100.0f),
                        (s32)MAX_ACCEL_M2 * (s32)(param->spd_acc.acc_xy / 100.0f),
                        (s32)MAX_ACCEL_M3 * (s32)(param->spd_acc.acc_z / 100.0f));
                    break;

                case MODE_CAL:
                    LOG(LOG_LOW, F("->MSG: JJAC:\r\n"));
                    m_robot.startGoHome();
                    break;

                case MODE_EMERGENCY:
                    LOG(LOG_LOW, F("->MSG: JJAE:\r\n"));
                    m_robot.disable();
                    //m_robot.dumpMotors();
                    break;
            }
            
            return ret;
        }

    private:
        CommInterface *m_pComm;
        u16            m_pwm;
};


CommInterface   *m_pComm[MAX_COMM];


/*
*****************************************************************************************
* setup
*****************************************************************************************
*/
void setup() {
    Serial.begin(SERIAL_BPS);

    setLogMask(LOG_LOW_ONLY);
    m_robot.setup();
    m_robot.disable();

    m_pComm[0] = new CommSerial(100);
    m_pComm[1] = NULL; //new CommUDP(100);
    for (u8 i  = 0; i < MAX_COMM; i++) {
        if (m_pComm[i]) {
            m_pComm[i]->getProtocol()->setCallback(new CommandHandler(m_pComm[i]));
        }
    }

    m_robot.setServo(SERVO_WRIST,   PIN_SERVO_WRIST,   SERVO_WRIST_MIN_PWM,   SERVO_WRIST_MAX_PWM);
    m_robot.setServo(SERVO_GRIPPER, PIN_SERVO_GRIPPER, SERVO_GRIPPER_MIN_PWM, SERVO_GRIPPER_MAX_PWM);
    m_robot.setSteper(MOT_A1, PIN_MOT_X_STEP, PIN_MOT_X_DIR, PIN_ENDSTOP_X, MAX_SPEED_M1, MAX_ACCEL_M1);
    m_robot.setSteper(MOT_A2, PIN_MOT_Y_STEP, PIN_MOT_Y_DIR, PIN_ENDSTOP_Y, MAX_SPEED_M2, MAX_ACCEL_M2);
    m_robot.setSteper(MOT_Z,  PIN_MOT_Z_STEP, PIN_MOT_Z_DIR, PIN_ENDSTOP_Z, MAX_SPEED_M3, MAX_ACCEL_M3);

    m_robot.configSpeed(MAX_SPEED_M1 / 10, MAX_SPEED_M2 / 10, MAX_SPEED_M3);
    m_robot.configAcceleration(MAX_ACCEL_M1 / 2, MAX_ACCEL_M2 / 2, MAX_ACCEL_M3);
    m_robot.updateSpeedAcc();

    m_robot.setAxis(0.0f, 0.0f);
    m_robot.setHeight(0.0f);
    m_robot.setTarget2Pos();

    m_robot.writeServo(SERVO_WRIST, 1010);
    m_robot.writeServo(SERVO_GRIPPER, 1500);

#if 0
    setAxis_1_2(0, 0);
    setSpeedAcc();
    adjustTrajectorySpeed();
    setAxis3(0);
#endif


/*
    char buf[10];
    long t1;
    long t2;
    float fout;
    s16   sout;
    float frad;
    s16   srad;

    for (int i = 0; i < 45; i++) {
        frad = DEG2RAD * i;
        t1   = micros();
        fout = cos(frad);
        fout = acos(fout) * RAD2DEG;
        t1   = micros() - t1;
        dtostrf(fout, 6, 3, buf);

        t2   = micros();
        sout = d4cos(i * 10);
        sout = ((s32)d4acos(sout) * RAD2DEG_EXP_2) / DEC_EXP_6;
        t2   = micros() - t2;
        
        LOG(F("%5d => math:%s, imath:%6d : T[%6ld / %6ld]\n"), i, buf, sout, t1, t2);
    }
*/
}


/*
*****************************************************************************************
* loop
*****************************************************************************************
*/
static long oldPosTS;
static long oldRptTS;
static long ts;
static bool isWorking;
static s16  timeStamp = 0;
static char message[80];
static u16  dt;
    
void loop() {
    u8 i;
    
    ts = micros();

    for (i  = 0; i < MAX_COMM; i++) {
        if (m_pComm[i]) {
            m_pComm[i]->loop();
        }
    }

    //
    // position control
    //
    dt = ts - oldPosTS;
    if (dt >= 1000) {
        oldPosTS = ts;
        isWorking = m_robot.positionControl(dt);

        if (m_ucTrajMax > 0) {
            if ((m_robot.getDistance(MOT_A1) < m_wTrajTolerances[MOT_A1]) && 
                (m_robot.getDistance(MOT_A2) < m_wTrajTolerances[MOT_A2]) && 
                (m_robot.getDistance(MOT_Z) < m_wTrajTolerances[MOT_Z])) {
                if (0 <= m_ucTrajIdx && m_ucTrajIdx <= m_ucTrajMax) {
                    m_robot.setAxis(m_dkTrajs[m_ucTrajIdx].axis1 / 100.0f, m_dkTrajs[m_ucTrajIdx].axis2 / 100.0f);
                    m_robot.updateSpeedAcc();
                    m_robot.adjustTrajectorySpeed();

                    if (m_dkTrajs[m_ucTrajIdx].axis3 != NODATA) {
                        m_robot.setHeight(m_dkTrajs[m_ucTrajIdx].axis3 / 100.0);
                    }
                    if (m_dkTrajs[m_ucTrajIdx].servoWrist != NODATA) {
                        m_robot.moveServo(SERVO_WRIST, m_dkTrajs[m_ucTrajIdx].servoWrist / 1000.0f);
                    }
                    if (m_dkTrajs[m_ucTrajIdx].servoGripper != NODATA) {
                        m_robot.moveServo(SERVO_GRIPPER, m_dkTrajs[m_ucTrajIdx].servoGripper / 1000.0f);
                    }
                    m_ucTrajIdx++;
                } else if (m_ucTrajIdx >= m_ucTrajMax) {
                    m_ucTrajMax = 0;
                    m_ucTrajIdx = 0;
                }
            }
        }
    }

#if 1
    //
    // report status
    //
    ts = millis();
    if (ts - oldRptTS >= 200) {
        s16 a1, a2, h, status;
        
        oldRptTS = ts;
        if (m_ucTrajIdx < 0) {
            status = 2;
        } else {
            if ((m_robot.getDistance(MOT_A1) < TOLERANCE_STOP) && 
                (m_robot.getDistance(MOT_A2) < TOLERANCE_STOP) && 
                (m_robot.getDistance(MOT_Z)  < TOLERANCE_STOP)) {
                status = 0;
            } else {
                status = 1;
            }
        }

        timeStamp++;
        if (timeStamp > 999) {
            timeStamp = 0;
        }

        //m_robot.testServo();
        //m_robot.dumpMotors();

#if 1
        m_robot.getStatus(&a1, &a2, &h);
        snprintf_P(message, sizeof(message), (const char *)F("$$%d,%d,%d,%u,%d,%d,%d,%d\r\n"), status, a1, a2, h, 0, 0, 100, timeStamp);
        for (i  = 0; i < MAX_COMM; i++) {
            if (m_pComm[i]) {
                m_pComm[i]->getProtocol()->sendStatus((u8*)message, strlen(message));
            }
        }
#endif
        
    }
#endif

}

