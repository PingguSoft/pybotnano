#ifndef _SERVO_DRV_H_
#define _SERVO_DRV_H_
#include <Arduino.h>
#include "config.h"


/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/
#define MAX_SERVOS              2

#define MIN_PWM               544
#define MAX_PWM              2400
#define DEFAULT_PWM          1500
#define REFRESH_INTERVAL    18000

#define SERVOS_PER_TIMER       10

#define SERVO_ENABLED       0x80

/*
*****************************************************************************************
* MACROS
*****************************************************************************************
*/


/*
*****************************************************************************************
* Class
*****************************************************************************************
*/
typedef struct {
    u8  pin;
    u16 tick;
    u16 minMicro;
    u16 maxMicro;

    volatile u8 *reg_out;
    u8  mask_out;
} servoi_t;

class ServoDrv {
    public:
        ServoDrv(void);
        s8   set(u8 idx, u8 pin, u16 min, u16 max);
        s8   getHandle(u8 pin);
        void write(s8 handle, u16 pwm);
        void enable(bool en)            { for (u8 i = 0; i < MAX_SERVOS; i++) enable(i, en); }
        void enable(s8 handle, bool en) { m_servos[handle].pin = en ? (m_servos[handle].pin | SERVO_ENABLED) : (m_servos[handle].pin & (~SERVO_ENABLED)); }
        bool isEnabled(s8 handle)       { return (m_servos[handle].pin & SERVO_ENABLED); }

        u16  process(u16 tcnt);
        servoi_t *get(s8 handle)        { return &m_servos[handle]; }

    private:
        s8          m_scIndex;
        u8          m_ucServos;
        u16         m_wStartCnt;
        servoi_t    m_servos[MAX_SERVOS];
};

#endif

