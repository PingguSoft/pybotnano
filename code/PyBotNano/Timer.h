#ifndef _TIMER_H_
#define _TIMER_H_
#include "common.h"

/*
*****************************************************************************************
* CONST
*****************************************************************************************
*/
// all timer prescaler=8, => 2Mhz tick
#define TMR8_MIN_NEXT       100     // 50us

/*
*****************************************************************************************
* MACROS
*****************************************************************************************
*/
#define mMicro2Tick(micro)  ((u16)micro << 1)
#define mTick2Micro(tick)   ((u16)tick  >> 1)


/*
*****************************************************************************************
* Class
*****************************************************************************************
*/
class TimerCallback {
    public:
        virtual u16 onTimer(u8 idx, u16 cnt) = 0;
};


class Timer {
    public:
        Timer(void);
        void  setup(void);
        void  setTimer(u8 idx, u16 period);
        void  setCallback(TimerCallback *pCallback) { m_pCallback = pCallback;               }
        
inline  u16   onTimer(u8 idx, u16 cnt)              { return m_pCallback->onTimer(idx, cnt); }
inline  u16   getPeriod(u8 idx)                     { return m_wPeriods[idx];                }
inline  void  setPeriod(u8 idx, u16 period)         { m_wPeriods[idx] = period;              }

        u16   getCnt(u8 idx);


    private:
        u16             m_wPeriods[4];
        TimerCallback   *m_pCallback;
};

#endif

