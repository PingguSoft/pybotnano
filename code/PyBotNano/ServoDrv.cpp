#include "utils.h"
#include "Timer.h"
#include "ServoDrv.h"

/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/


/*
*****************************************************************************************
* TYPES
*****************************************************************************************
*/


/*
*****************************************************************************************
* MACROS
*****************************************************************************************
*/


/*
*****************************************************************************************
* VARIABLES
*****************************************************************************************
*/

/*
*****************************************************************************************
* TIMERS
*****************************************************************************************
*/
ServoDrv::ServoDrv(void) {
    m_scIndex  = -1;
    enable(false);
}

s8 ServoDrv::getHandle(u8 pin) {
    for (u8 i = 0; i < MAX_SERVOS; i++) {
        if ((m_servos[i].pin & (~SERVO_ENABLED)) == pin)
            return i;
    }

    return -1;
}

s8 ServoDrv::set(u8 idx, u8 pin, u16 min, u16 max) {
    s8 h = -1;
    
    if (idx < MAX_SERVOS) {
        m_servos[idx].pin      = pin | SERVO_ENABLED;
        m_servos[idx].minMicro = constrain(min, MIN_PWM, MAX_PWM);
        m_servos[idx].maxMicro = constrain(max, MIN_PWM, MAX_PWM);
        m_servos[idx].tick     = mMicro2Tick(DEFAULT_PWM);

        m_servos[idx].reg_out  = portOutputRegister(digitalPinToPort(pin));
        m_servos[idx].mask_out = digitalPinToBitMask(pin);
        
        h = idx;
        pinMode(pin, OUTPUT);
    }

    return h;
}

void ServoDrv::write(s8 handle, u16 pwm) {
    pwm = constrain(pwm, m_servos[handle].minMicro, m_servos[handle].maxMicro);
    m_servos[handle].tick = mMicro2Tick(pwm);
}

#define mDiff(last, start) (((last) > (start)) ? ((last) - (start)) : ((65535 - (start) + 1) + (last)))

u16 ServoDrv::process(u16 tcnt) {
    servoi_t *pServo;
    u16       next = mMicro2Tick(REFRESH_INTERVAL);
    
    if (m_scIndex < 0) {
        m_wStartCnt = tcnt;
    } else {
        if (m_scIndex < MAX_SERVOS) {
            pServo = &m_servos[m_scIndex];
            mClrMask(*pServo->reg_out, pServo->mask_out);
        }
    }

//    LOG(LOG_LOW, F("%5u\n"), tcnt);

    m_scIndex++;
    if (m_scIndex < MAX_SERVOS) {
        pServo = &m_servos[m_scIndex];
        next   = pServo->tick;
        if (isEnabled(m_scIndex)) {
            mSetMask(*pServo->reg_out, pServo->mask_out);
        }
    } else {
        u16 diff = mDiff(tcnt, m_wStartCnt);
        if (diff < mMicro2Tick(REFRESH_INTERVAL)) {
            next = mMicro2Tick(REFRESH_INTERVAL) - diff;
        } else {
            next = mMicro2Tick(30);
        }
        m_scIndex = -1;
    }

    return next;
}

