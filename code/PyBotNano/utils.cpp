/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#include <stdarg.h>
#include "utils.h"


static u32 rand_seed = 0xb2c54a2ful;
// Linear feedback shift register with 32-bit Xilinx polinomial x^32 + x^22 + x^2 + x + 1
static const u32 LFSR_FEEDBACK = 0x80200003ul;
static const u32 LFSR_INTAP = 32-1;

static u16 m_wLogMask = LOG_NONE;

void setLogMask(u16 mask) {
    m_wLogMask = mask;
}

static void update_lfsr(uint32_t *lfsr, uint8_t b)
{
    for (int i = 0; i < 8; ++i) {
        *lfsr = (*lfsr >> 1) ^ ((-(*lfsr & 1u) & LFSR_FEEDBACK) ^ ~((uint32_t)(b & 1) << LFSR_INTAP));
        b >>= 1;
    }
}

u32 rand32_r(u32 *seed, u8 update)
{
    if (!seed)
        seed = (u32*)&rand_seed;
    update_lfsr(seed, update);
    return *seed;
}

u32 rand32()
{
    return rand32_r(0, 0);
}

char *catD2str(double d, s8 width, u8 precision, char *dest, u16 dest_size, bool clr) {
    char buf[width * 2];

    if (clr) {
        memset(dest, 0, dest_size);
    }
    dtostrf(d, width, precision, buf);
    if (!clr) {
        strcat_P(dest, (const char*)F(", "));
    }
    strcat(dest, buf);

    return dest;
}

#if __FEATURE_DEBUG__
void LOG(u16 mask, char *fmt, ... ) {
    if (m_wLogMask & mask) {
        char    buf[128];        
        va_list args;
        
        va_start (args, fmt );
        vsnprintf(buf, sizeof(buf), fmt, args);
        va_end (args);
        Serial.print(buf);
    }
}

void LOG(u16 mask, const __FlashStringHelper *fmt, ... ) {
    if (m_wLogMask & mask) {
        char    buf[128];
        va_list args;
        
        va_start (args, fmt);
#ifdef __AVR__
        vsnprintf_P(buf, sizeof(buf), (const char *)fmt, args); // progmem for AVR
#else
        vsnprintf(buf, sizeof(buf), (const char *)fmt, args); // for the rest of the world
#endif

        va_end(args);
        Serial.print(buf);
    }
}

void DUMP(u16 mask, char *name, u8 *data, u16 cnt)
{
    u8  i;
    u8  b;
    u16 addr = 0;

    LOG(mask, F("-- %s buf size : %d -- \n"), name, cnt);
    while (cnt) {
        LOG(mask, F("%08x - "), addr);

        for (i = 0; (i < 16) && (i < cnt); i ++) {
            b = *(data + i);
            LOG(mask, F("%02x "), b);
        }

        LOG(mask, F(" : "));
        for (i = 0; (i < 16) && (i < cnt); i ++) {
            b = *(data + i);
            if ((b > 0x1f) && (b < 0x7f))
                LOG(mask, "%c", b);
            else
                LOG(mask, ".");
        }
        LOG(mask, "\n");
        data += i;
        addr += i;
        cnt  -= i;
    }
}
#endif