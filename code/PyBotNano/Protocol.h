#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_
#include "common.h"


/*
*****************************************************************************************
* MACROS
*****************************************************************************************
*/
#define NODATA  -20000

typedef enum {
    MODE_NONE  = 0,
    MODE_HELLO,         // say hello
    MODE_DK,            // direct  kinematic
    MODE_IK,            // inverse kinematic
    MODE_TRAJECTORY,    // trajectory (direct kinematic)
    MODE_SET,           // set speed and acc
    MODE_CAL,           // calibration
    MODE_EMERGENCY,     // emergency stop
    MODE_TEST,
    MODE_LOG_MASK,
} MODE_T;

typedef struct {
    u8      cmd;
    u8      size;
} CMD_T;

struct param_dk {
    s16 axis1;
    s16 axis2;
    s16 axis3;
    s16 servoWrist;     // wrist
    s16 servoGripper;   // gripper
};

struct param_ik {
    s16 x;
    s16 y;
    s16 z;
    s16 servoWrist;     // wrist
    s16 servoGripper;   // gripper
    s16 elbow;          // 0:positive, 1:negative
};   

struct param_tj {
    s16 x;
    s16 y;
    s16 z;
    s16 servoWrist;
    s16 servoGripper;
    s16 elbow;
    s16 index;
    s16 last;           // 1:last index
};

struct param_spd_acc {
    s16 spd_xy;
    s16 spd_z;
    s16 acc_xy;
    s16 acc_z;
    s16 spd_tj;
};

union params {
    u16                     wParams[8];
    struct param_dk         dk;
    struct param_ik         ik;
    struct param_tj         tj;
    struct param_spd_acc    spd_acc;
};

/*
*****************************************************************************************
* Class
*****************************************************************************************
*/
class ProtocolCallback {
    public:
        virtual ~ProtocolCallback() { }
        virtual s8 onCommand(u8 cmd, u8 *pData, u8 size) = 0;
};

class ProtocolInterface {
    public:
        virtual ~ProtocolInterface() { }
//        virtual int  getHandle(void)           = 0;
        virtual void write(u8 *pBuf, u16 size) = 0;
};

class Protocol {
    public:
        Protocol(u8 m_wPacketSize);
        ~Protocol();
        
        void setInterface(ProtocolInterface *pInterface);
        void setCallback(ProtocolCallback *pCallback);
        void poll();
        void processRx(u8 *pBuf, u16 size);
        void sendStatus(u8 *pData, u8 size);

    private:
        enum STATE_T {
            STATE_IDLE,
            STATE_HEADER_1ST_J, // 'J'
            STATE_HEADER_2ND_J, // 'J'
            STATE_HEADER_A,     // 'A'
            STATE_HEADER_CMD,   // 'H', 'M', 'I', 'T', 'S', 'C', E', 
        };
       
        u8           *m_pRxPackets;
        u8           m_ucPacketSize;
        u8           m_ucSeq;
        u8           m_ucSubSeq;
        
        enum STATE_T m_stateRx;
        u8           m_ucOffset;
        u8           m_ucDataSize;
        u8           m_ucCmd;
        ProtocolCallback  *m_pCallback;
        ProtocolInterface *m_pInterface;
};

#endif

