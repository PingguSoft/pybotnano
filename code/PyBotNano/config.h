/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "common.h"


/*
*****************************************************************************************
* OPTIONS
*****************************************************************************************
*/
#define __FEATURE_DEBUG__           1


/*
*****************************************************************************************
* CONFIGURATION
*****************************************************************************************
*/
#define RAD2DEG                     57.2957795                          // 180 / pi
#define DEG2RAD                     0.01745329251994329576923690768489  // pi  / 180


//
// timer configuration
//
#define TIMER_PRESCALE              8
#define TIMER_CLK                   2000000     // 0.5us
#define TIMER_MINIMUN_PERIOD        400         // 0.5us * 400   = 200us = 5KHz
#define TIMER_MAXIMUN_PERIOD        30000       // 0.5us * 30000 = 15ms

//
// robot configuration
//
#define ROBOT_ARM1_LENGTH           91.61f      //92.84  FISRT SEGMENT  
#define ROBOT_ARM2_LENGTH           105.92f     //106.79 SECOND SEGMENT
#define ROBOT_INITIAL_ANGLE1        0
#define ROBOT_INITIAL_ANGLE2        0
#define ROBOT_INITIAL_HEIGHT        0


//
// steps calcuations
//
// This depends on the pulley teeth and microstepping.
// full = 1.8 degree / step, microstepping = 16 then  200 * 16 = 3200 steps for 1 rotation
// For 16/72 teeth        8.8888888888[3200 / 360] * 4.5[72 / 16] = 40.0
#define M1_AXIS_STEPS_PER_UNIT      40.0f

// For 16/62, 33/62 teeth 8.8888888888[3200 / 360] * 7.2803[62 /33 * 62 / 16] = 64.713
#define M2_AXIS_STEPS_PER_UNIT      64.713f

// Steps to mm for axisZ  200 * 16 = 3200
// https://blog.prusaprinters.org/calculator/
// pitch 2
// lead 2 (1 thread) = 3200 / 2 = 1600
// lead 4 (2 thread) = 3200 / 4 =  800
// lead 8 (4 thread) = 3200 / 8 =  400
#define M3_AXIS_STEPS_PER_UNIT      400 //396

// Correccion for robot structure (diference between reductions on each axis)
#define AXIS2_AXIS1_CORRECTION      (33.0f / 62.0f)
#define M1_M2_STEP_FACTOR           (M1_AXIS_STEPS_PER_UNIT / M2_AXIS_STEPS_PER_UNIT)


//
// limitations
//
// THIS VALUES DEPENDS ON THE MOTORS, PULLEYS AND ROBOT CONSTRUCTION
// Maximun motor acceleration in (steps/seg2)/1000 [acceleration in miliseconds] 30 => 30.000 steps/sec2
#define MAX_ACCEL_M1                40
#define MAX_ACCEL_M2                MAX_ACCEL_M1 / M1_M2_STEP_FACTOR
#define MAX_ACCEL_M3                30

#define MIN_ACCEL_M1                3
#define MIN_ACCEL_M2                5
#define MIN_ACCEL_M3                4

// Maximun speed in steps/seg (max 32765)
#define MAX_SPEED_M1                16000
#define MAX_SPEED_M2                (MAX_SPEED_M1 / M1_M2_STEP_FACTOR)
#define MAX_SPEED_M3                20000

#define MIN_SPEED_M1                1000
#define MIN_SPEED_M2                1000
#define MIN_SPEED_M3                1000

#define ROBOT_NO_MOVE               -1000
#define ROBOT_MIN_ANGLE1            -120.0
#define ROBOT_MIN_ANGLE2            -140.0
#define ROBOT_MIN_HEIGHT            0               // mm
#define ROBOT_MAX_ANGLE1            120.0
#define ROBOT_MAX_ANGLE2            140.0
#define ROBOT_MAX_HEIGHT            120             // mm
#define ROBOT_AXIS_DEFINITION       (-90 * DEG2RAD) // -90 degrees of axis rotation for our robot configuration
#define ROBOT_ABS_MAX_ANGLE1        121             // max degrees
#define ROBOT_ABS_MAX_ANGLE2        146             // max degress
#define ROBOT_ABS_MAX_HEIGHT        150             // mm



#define TOLERANCE_STOP              4               // Tolerance to check if the robot arrive at point
#define TOLERANCE_TRAJECTORY_M1     int(M1_AXIS_STEPS_PER_UNIT * 2)
#define TOLERANCE_TRAJECTORY_M2     int(M2_AXIS_STEPS_PER_UNIT * 2)
#define TOLERANCE_TRAJECTORY_M3     int(M3_AXIS_STEPS_PER_UNIT * 2)

//
// servo configuration
//
#define SERVO_WRIST_NEUTRAL         1500
#define SERVO_WRIST_MIN_PWM         1000
#define SERVO_WRIST_MAX_PWM         1900

#define SERVO_GRIPPER_NEUTRAL       1500
#define SERVO_GRIPPER_MIN_PWM       1000
#define SERVO_GRIPPER_MAX_PWM       1900

/*
*****************************************************************************************
* RULE CHECK
*****************************************************************************************
*/
#define __STD_SERIAL__              1


/*
*****************************************************************************************
* PINS
*****************************************************************************************
*/
#define PIN_MOT_ENABLE              8


// MOTOR1
#define PIN_MOT_X_STEP              5
#define PIN_MOT_X_DIR               2

// MOTOR2
#define PIN_MOT_Y_STEP              6
#define PIN_MOT_Y_DIR               3

// MOTOR3
#define PIN_MOT_Z_STEP              7
#define PIN_MOT_Z_DIR               4

#if defined(__AVR_ATmega328P__)
    #define PIN_ENDSTOP_X               9
    #define PIN_ENDSTOP_Y               10
    #define PIN_ENDSTOP_Z               11

    #define PIN_SERVO_WRIST             12
    #define PIN_SERVO_GRIPPER           14
    #define PIN_LED                     13
#elif defined(__AVR_ATmega32U4__)
    #define PIN_ENDSTOP_X               A3
    #define PIN_ENDSTOP_Y               A2
    #define PIN_ENDSTOP_Z               A1

    #define PIN_SERVO_WRIST             15
    #define PIN_SERVO_GRIPPER           14
    #define PIN_LED                     16    
#endif

/*
#define PIN_ABORT                   A0
#define PIN_HOLD                    A1
#define PIN_RESUME                  A2
#define PIN_COOLANT                 A3
*/

/*
*****************************************************************************************
* COMMON SETTINGS
*****************************************************************************************
*/
#define SERIAL_BPS                  115200

#endif
