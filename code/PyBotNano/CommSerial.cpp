#include "utils.h"
#include "CommInterface.h"

class GSInterface : public ProtocolInterface {
    public:
        GSInterface() {
        }


        GSInterface(CommSerial* x):m_pParent(x) {
        }

        virtual ~GSInterface() {
        }

        virtual void write(u8 *pBuf, u16 size) {
            Serial.write(pBuf, size);
        }

    private:
        CommSerial  *m_pParent;
};

int CommSerial::loop() {
    u16 count = Serial.available();

    if (count > 0) {
        count = Serial.readBytes(m_ucRxBuf, count);
        //DUMP("rx", m_ucRxBuf, count);
        
        getProtocol()->processRx(m_ucRxBuf, count);
    }
}

CommSerial::CommSerial(int arg) : CommInterface(80) {
    getProtocol()->setInterface(new GSInterface(this));
    setConnected(TRUE);
}

CommSerial::~CommSerial() {
}

