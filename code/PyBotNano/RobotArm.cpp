#include <Arduino.h>
#include <stdarg.h>
#include <math.h>
#include "common.h"
#include "config.h"
#include "utils.h"
#include "RobotArm.h"

/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/
#define OVERSHOOT_COMPENSATION  20


/*
*****************************************************************************************
* TYPES
*****************************************************************************************
*/


/*
*****************************************************************************************
* MACROS
*****************************************************************************************
*/


/*
*****************************************************************************************
* VARIABLES
*****************************************************************************************
*/
static const s16 PROGMEM TBL_MIN_ANGLE[] = {
    -140, -140, -140, -140, -140, -140, -140, -130, -120, -110,  -95,  -88
};

static const s16 PROGMEM TBL_MAX_ANGLE[] = {
    88, 95, 110, 120, 130, 140, 140, 140, 140, 140, 140, 140
};


/*
*****************************************************************************************
* UTILITY FUNCTIONS
*****************************************************************************************
*/
void RobotArm::_delay1uS(void) {
    // 16 single cycle instructions = 1us at 16Mhz    
    __asm__ __volatile__ (
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop");
}

s16 RobotArm::getDistance(u8 idx) {
    stepper_t *pStepper = _getStepper(idx);

    return mABS(pStepper->tgt_pos - pStepper->pos);
}

void RobotArm::getStatus(s16 *A1, s16 *A2, s16 *height) {
    *A1     = (_getStepper(MOT_A1)->pos * 10) / M1_AXIS_STEPS_PER_UNIT;
    *A2     = (_getStepper(MOT_A2)->pos * 10) / M2_AXIS_STEPS_PER_UNIT - (*A1 * AXIS2_AXIS1_CORRECTION);
    *height = (_getStepper(MOT_Z)->pos  * 10) / M3_AXIS_STEPS_PER_UNIT;
    if (*height < 0) {
        *height = 0;
    }
}

/*
*****************************************************************************************
* 
*****************************************************************************************
*/
RobotArm::RobotArm(void) {
}

void RobotArm::setup(void) {
    pinMode(PIN_MOT_ENABLE, OUTPUT);
    pinMode(PIN_ENDSTOP_X, INPUT_PULLUP);
    pinMode(PIN_ENDSTOP_Y, INPUT_PULLUP);
    pinMode(PIN_ENDSTOP_Z, INPUT_PULLUP);
    pinMode(PIN_LED, OUTPUT);
   
    m_timer.setup();
    m_timer.setCallback(this);
    m_timer.setTimer(3, mMicro2Tick(REFRESH_INTERVAL));
}

void RobotArm::setServo(u8 idx, u8 pin, u16 min, u16 max) {
    m_servo.set(idx, pin, min, max);
}

void RobotArm::moveServo(u8 idx, float percent) {
    u16      range;
    u16      pwm;
    servoi_t *servo = m_servo.get(idx);

    range = servo->maxMicro - servo->minMicro;
    m_servo.write(idx, (percent * range) + servo->minMicro);
}

void RobotArm::writeServo(u8 idx, u16 pwm) {
    m_servo.write(idx, pwm);
}



u16 pwm = 1500;
s8  inc = 10;

void RobotArm::testServo(void) {
    s8 h = m_servo.getHandle(PIN_SERVO_GRIPPER);
    servoi_t *servo = m_servo.get(h);

    LOG(LOG_LOW_ONLY, F("pwm:%5u [%5u-%5u]\n"), pwm, servo->minMicro, servo->maxMicro);
    m_servo.write(h, pwm);
    if (pwm > servo->maxMicro || pwm < servo->minMicro) {
        inc = -inc;
    }
    pwm += inc;
}


u16 RobotArm::onTimer(u8 idx, u16 cnt) {
    u16 ret = 0;
    
    if (idx < 3) {
        _step(idx);
    } else {
        ret = m_servo.process(cnt);
//        LOG(LOG_LOW, F("%u\n"), cnt); 
    }

    return ret;
}

void RobotArm::setSteper(u8 idx, u8 pin_step, u8 pin_dir, u8 pin_endstop, s16 max_speed, s16 max_acc) {
    stepper_t *pStepper = _getStepper(idx);
    
    memset(pStepper, 0, sizeof(stepper_t));
    pinMode(pin_step, OUTPUT);
    pStepper->pin_step     = pin_step;
    pStepper->reg_out_step = portOutputRegister(digitalPinToPort(pin_step));
    pStepper->mask_step    = digitalPinToBitMask(pin_step);
    mClrMask(*pStepper->reg_out_step, pStepper->mask_step);

    pinMode(pin_dir, OUTPUT);
    pStepper->pin_dir     = pin_dir;        
    pStepper->reg_out_dir = portOutputRegister(digitalPinToPort(pin_dir));
    pStepper->mask_dir    = digitalPinToBitMask(pin_dir);
    mClrMask(*pStepper->reg_out_dir, pStepper->mask_dir);

    pinMode(pin_endstop, INPUT_PULLUP);
    pStepper->is_stopped     = false;
    pStepper->pin_endstop    = pin_endstop;        
    pStepper->reg_in_endstop = portInputRegister(digitalPinToPort(pin_endstop));
    pStepper->mask_endstop   = digitalPinToBitMask(pin_endstop);

    pStepper->max_speed   = max_speed;
    pStepper->conf_speed  = max_speed;
    pStepper->conf_accel  = max_acc;

    pStepper->acc_dt      = 0;
    pStepper->pos_last    = -32767;
}

void RobotArm::disable(void) {
    digitalWrite(PIN_MOT_ENABLE, HIGH);
//    m_servo.enable(false);
}

void RobotArm::enable(void) {
    digitalWrite(PIN_MOT_ENABLE, LOW);
//    m_servo.enable(true);
}

void RobotArm::_step(u8 idx) {
    stepper_t *pStepper = _getStepper(idx);
    
    if (pStepper->dir == 0 || (pStepper->is_stopped && pStepper->dir == -1))
        return;

    mSetMask(*pStepper->reg_out_step, pStepper->mask_step);
    _delay1uS();
    mClrMask(*pStepper->reg_out_step, pStepper->mask_step);
    _delay1uS();
    pStepper->pos += pStepper->dir;

    if (pStepper->pin_endstop != -1 && pStepper->dir == -1) {
        pStepper->is_stopped = !mIsMaskSet(*pStepper->reg_in_endstop, pStepper->mask_endstop);
    } else {
        pStepper->is_stopped = false;
    }
}

void RobotArm::_step(u8 idx, s8 dir) {
    stepper_t *pStepper = _getStepper(idx);
    
    if (dir == 0 || (pStepper->is_stopped && dir == -1))
        return;

    if (dir == 1) {
        mSetMask(*pStepper->reg_out_dir, pStepper->mask_dir);
    } else {
        mClrMask(*pStepper->reg_out_dir, pStepper->mask_dir);
    }

    mSetMask(*pStepper->reg_out_step, pStepper->mask_step);    
    _delay1uS();
    mClrMask(*pStepper->reg_out_step, pStepper->mask_step);
    _delay1uS();

    if (pStepper->pin_endstop != -1 && dir == -1) {
        pStepper->is_stopped = !mIsMaskSet(*pStepper->reg_in_endstop, pStepper->mask_endstop);
    } else {
        pStepper->is_stopped = false;
    }
}

void RobotArm::_setSpeed(u8 idx, stepper_t *pStepper, s16 speed, u16 dt, s16 overshoot_comp) {
    long    timer_period;
    s16     accel;
    
    speed          = constrain(speed, (s16)-pStepper->max_speed, (s16)pStepper->max_speed);
    overshoot_comp = constrain(overshoot_comp, -10, 10);
    accel          = (((long)pStepper->accel * dt) / 1000) + overshoot_comp;

    s16 diff = speed - pStepper->speed;
    
    if (mABS(diff) > accel) {
        pStepper->speed += (diff > 0) ? accel : (-accel);
    } else {
        pStepper->speed  = speed;
    }

    if (pStepper->speed == 0 && pStepper->dir != 0) {
        mSetMask(*pStepper->reg_out_dir, pStepper->mask_dir);
        pStepper->dir = 0;  // stop
    } else if (pStepper->speed > 0 && pStepper->dir != 1) {
        mSetMask(*pStepper->reg_out_dir, pStepper->mask_dir);
        pStepper->dir = 1;
    } else if (pStepper->speed < 0 && pStepper->dir != -1) {
        mClrMask(*pStepper->reg_out_dir, pStepper->mask_dir);
        pStepper->dir = -1;
    }

    if (pStepper->speed == 0) {
        timer_period = 0;
    } else {
        timer_period = (TIMER_CLK / pStepper->speed);
        if (timer_period < 0) {
            timer_period = -timer_period;
        }
    }

    if (0 != timer_period && timer_period < TIMER_MINIMUN_PERIOD) {
        timer_period = TIMER_MINIMUN_PERIOD;
    } else if (timer_period > TIMER_MAXIMUN_PERIOD) {
        timer_period = TIMER_MAXIMUN_PERIOD;
    }
   
    m_timer.setTimer(idx, timer_period);
    LOG(LOG_MID, F("%d, spd:%5d/%5d, diff:%5d, period:%5u\r\n"), idx, speed, pStepper->speed, diff, timer_period);
}


void RobotArm::adjustSpeed(void) {
    float diff_M1;
    float diff_M2;

    // Speed adjust to draw straight lines
    diff_M1 = mABS((float) m_steppers[1].tgt_pos - (float) m_steppers[1].pos);
    diff_M2 = mABS((float) m_steppers[2].tgt_pos - (float) m_steppers[2].pos) * M1_M2_STEP_FACTOR;
    if (diff_M1 >= diff_M2) { // Wich axis will be slower?
        // X axis is the main axis
        m_steppers[0].tgt_speed = MAX_SPEED_M1;
        m_steppers[1].tgt_speed = (float) MAX_SPEED_M2 * diff_M2 / diff_M1;
    } else {
        m_steppers[1].tgt_speed = MAX_SPEED_M2;
        m_steppers[0].tgt_speed = (float) MAX_SPEED_M1 * diff_M1 / diff_M2;
    }
}

// Set speed in steps/sec
void RobotArm::configSpeed(s32 speed0, s32 speed1, s32 speed2) {
    LOG(LOG_MID, F("s:%ld / %ld / %ld\r\n"), speed0, speed1, speed2);

    m_steppers[0].conf_speed = constrain(speed0, 0, MAX_SPEED_M1);
    m_steppers[1].conf_speed = constrain(speed1, 0, MAX_SPEED_M2);
    m_steppers[2].conf_speed = constrain(speed2, 0, MAX_SPEED_M3);
}

// Set acceleration
void RobotArm::configAcceleration(s32 acc0, s32 acc1, s32 acc2) {
    LOG(LOG_MID, F("a:%ld / %ld / %ld\r\n"), acc0, acc1, acc2);

    m_steppers[0].conf_accel = constrain(acc0, MIN_ACCEL_M1, MAX_ACCEL_M1);
    m_steppers[1].conf_accel = constrain(acc1, MIN_ACCEL_M2, MAX_ACCEL_M2);
    m_steppers[2].conf_accel = constrain(acc2, MIN_ACCEL_M3, MAX_ACCEL_M3);
}

void RobotArm::updateSpeedAcc(void) {
    for (u8 i = 0; i < 3; i++) {
        m_steppers[i].tgt_speed = m_steppers[i].conf_speed;
        m_steppers[i].tgt_accel = m_steppers[i].conf_accel;
        LOG(LOG_MID, F("%d => conf_speed:%5d, conf_accel:%5d\r\n"), i, m_steppers[i].conf_speed, m_steppers[i].conf_accel);
    }
}

bool RobotArm::positionControl(u16 dt) {
    s32         dist;
    bool        isWorking = false;
    s16         overshoot;
    s32         pos_stop;
    s32         pos;
    stepper_t   *pStepper;
    static      u8 ctr = 0;

    for (u8 i = 0; i < 3; i++) {
        pStepper = _getStepper(i);

        noInterrupts();
        pos  = pStepper->pos;
        interrupts();

        pStepper->acc_dt += dt;
        LOG(LOG_MID, F("%d, dt:%6u / %6u\r\n"), i, pStepper->acc_dt, m_timer.getPeriod(i));
        
        if (pStepper->dir != 0 && pos == pStepper->pos_last) {
            isWorking = true;
            continue;
        }
        pStepper->pos_last = pos;
        pStepper->accel    = pStepper->tgt_accel;

        // We first check if we need to start decelerating in order to stop at the final position
        // calculate the distance to decelerate until stop at target speed
        // kinematic formula   d = (sqr(v1)-sqr(v0))/(2*a) because v1=0 and acc(a) is negative in deceleration => d = sqr(v1)/2a
        // We calculate the stop position and check if we need to start decelerating right now...
        dist = (s32)pStepper->speed * (s32)pStepper->speed;
        dist = dist / (2000 * (s32)pStepper->accel);

        if (pStepper->speed > 0) {
            pos_stop = pos + dist;
        } else {
            pos_stop = pos - dist;
        }

        if (i == 2)
        LOG(LOG_MID, F("%d, P:%5ld/%5ld/%5ld [%3d] dist:%5ld, S:%5d/%5d, A:%5d, dt:%5d PRD:%7u\r\n"),
            i, pos, pos_stop, pStepper->tgt_pos, pStepper->dir, dist,
            pStepper->speed, pStepper->tgt_speed, pStepper->accel, 
            pStepper->acc_dt, m_timer.getPeriod(i));

        if (pStepper->is_stopped) {
            pStepper->speed   = 0;
            pStepper->accel   = 0;
            pStepper->pos     = 0;
            pStepper->tgt_pos = 0;
        }
    
        if (pStepper->tgt_pos > pos) {                                              // Positive move
            if (pos_stop >= pStepper->tgt_pos) {                                    // Start decelerating?      
                overshoot = pos_stop - pStepper->tgt_pos;
                if (overshoot > OVERSHOOT_COMPENSATION) {                           // Increase decelaration a bit (overshoot compensation)
                    LOG(LOG_MID, F("%d, ->S1\r\n"), i);
                    _setSpeed(i, pStepper, 0, pStepper->acc_dt, overshoot / OVERSHOOT_COMPENSATION);
                }
                LOG(LOG_MID, F("%d, ->S2\r\n"), i);
                pStepper->is_stopping = true;
                _setSpeed(i, pStepper, 0, pStepper->acc_dt, 0);                     // The deceleration ramp is done inside the _setSpeed function
            } else {
                LOG(LOG_MID, F("%d, ->S3\r\n"), i);
                pStepper->is_stopping = false;
                _setSpeed(i, pStepper, pStepper->tgt_speed, pStepper->acc_dt, 0);   // The aceleration ramp is done inside the _setSpeed function
            }
        } else {                                                                    // Negative move
            if (pos_stop <= pStepper->tgt_pos) {                                    // Start decelerating?
                overshoot = pStepper->tgt_pos - pos_stop;
                if (overshoot > OVERSHOOT_COMPENSATION) {                           // Increase decelaration a bit (overshoot compensation)
                    LOG(LOG_MID, F("%d, ->S4\r\n"), i);
                    _setSpeed(i, pStepper, 0, pStepper->acc_dt, overshoot / OVERSHOOT_COMPENSATION);
                }  
                LOG(LOG_MID, F("%d, ->S5\r\n"), i);
                pStepper->is_stopping = true;
                _setSpeed(i, pStepper, 0, pStepper->acc_dt, 0);
            } else {
                LOG(LOG_MID, F("%d, ->S6\r\n"), i);
                pStepper->is_stopping = false;
                _setSpeed(i, pStepper, -pStepper->tgt_speed, pStepper->acc_dt, 0);
            }
        }

        if (pStepper->dir != 0) {
            isWorking = true;
        }
        pStepper->acc_dt = 0;

        if (pStepper->is_stopped) {
            pStepper->is_stopped = false;
        }
    }


    if (m_isCal && !isWorking) {
        doNextGoHome();
    }

    return isWorking;
}

void RobotArm::dumpMotors(void) {
    for (u8 i = 0; i < 3; i++) {
        LOG(LOG_LOW, F("m%d => P:%5ld/%5ld [%3d] S:%5d/%5d, A:%5d, tmr:%5d\n"),
            i, m_steppers[i].pos, m_steppers[i].tgt_pos, m_steppers[i].dir, 
            m_steppers[i].speed, m_steppers[i].tgt_speed, m_steppers[i].accel,
            m_timer.getPeriod(i));
    }
    LOG(LOG_LOW, F("----------\n"));
}

static const cal_info_t PROGMEM TBL_CAL_INFO[] = {
    {             ROBOT_NO_MOVE,              ROBOT_NO_MOVE,                    10 },    // lift +z a little
    {             ROBOT_NO_MOVE,              ROBOT_NO_MOVE, -ROBOT_ABS_MAX_HEIGHT },    // move -z until endpoint
/*
    {                        20,                         20,         ROBOT_NO_MOVE },    // move +a1, +a2 a little
    { -ROBOT_ABS_MAX_ANGLE1 * 2,              ROBOT_NO_MOVE,         ROBOT_NO_MOVE },    // move -a1 until endpoint
    { -ROBOT_ABS_MAX_ANGLE1 * 2,  -ROBOT_ABS_MAX_ANGLE2 * 2,         ROBOT_NO_MOVE },    // move -a2 until endpoint
    {             ROBOT_NO_MOVE,              ROBOT_NO_MOVE,         ROBOT_NO_MOVE },    // mark zero point

*/

/*
    { MAX_SPEED_M1 / 12, MAX_SPEED_M2 /  4, MAX_SPEED_M3, -ROBOT_ABS_MAX_ANGLE1, ROBOT_INITIAL_ANGLE2 + 90, 0 },
    { MAX_SPEED_M1 /  4, MAX_SPEED_M2 / 10, MAX_SPEED_M3,  ROBOT_INITIAL_ANGLE1, ROBOT_INITIAL_ANGLE2,      0 },
    { MAX_SPEED_M1 /  4, MAX_SPEED_M2 / 10, MAX_SPEED_M3,  ROBOT_INITIAL_ANGLE1, ROBOT_ABS_MAX_ANGLE2,      0 },
    { MAX_SPEED_M1 /  4, MAX_SPEED_M2 /  4, MAX_SPEED_M3,  ROBOT_INITIAL_ANGLE1, ROBOT_INITIAL_ANGLE2,      0 },
*/    
};

void RobotArm::startGoHome(void) { 
    m_isCal = true; 
    m_ucCalIdx = 0;

    configSpeed(MAX_SPEED_M1 / 12, MAX_SPEED_M2 /  4, MAX_SPEED_M3 / 4);
    updateSpeedAcc();
    enable();
}

void RobotArm::doNextGoHome(void) {
    cal_info_t info;

    if (m_ucCalIdx == sizeof(TBL_CAL_INFO) / sizeof(cal_info_t)) {
        m_isCal = false;
        
        setAxis(0.0f, 0.0f, false);
        setHeight(0.0f, false);
        setTarget2Pos();
        LOG(LOG_LOW, F("!!! CAL FINISHED !!!\n"));
        return;
    }
    
    PROGMEM_read(&TBL_CAL_INFO[m_ucCalIdx], info);
    LOG(LOG_LOW, F("cal:%d, a1:%5d, a2:%5d, z:%5d\n"), m_ucCalIdx, info.angle1, info.angle2, info.pos_z);

    if (info.angle1 > ROBOT_NO_MOVE && info.angle2 <= ROBOT_NO_MOVE) {
        setAxis(info.angle1, false);
    } else if (info.angle1 > ROBOT_NO_MOVE && info.angle2 > ROBOT_NO_MOVE) {
        setAxis(info.angle1, info.angle2, false);
    }
    
    if (info.pos_z > ROBOT_NO_MOVE) {
        setHeight(info.pos_z, false);
    }
    m_ucCalIdx++;
}

#if 0
void RobotArm::_controlLoop(CommInterface *comm) {
    bool isWorking;
    long ts, oldTS;
    u16  diff;
    
    isWorking = true;
    oldTS = micros();
    while (isWorking) {
        if (comm) {
            comm->loop();
        }
        ts   = micros();
        diff = ts - oldTS;
        if (diff > 1000) {
            isWorking = positionControl(diff);
            oldTS = ts;
        }
    }
}

void RobotArm::motorsCalibration(CommInterface *comm) {
    // Enable motors
    digitalWrite(PIN_MOT_ENABLE, LOW);

    // MOTOR1
    // Robot doNextGoHome... 
    // moving robot to both ends until we loose some steps (brute method because the robot dont have final pos switches)
    // Force the robot to loose steps and reach the final pos
    configSpeed(MAX_SPEED_M1 / 12, MAX_SPEED_M2 / 4, MAX_SPEED_M3);
    updateSpeedAcc();
    
    setAxis(-ROBOT_ABS_MAX_ANGLE1, ROBOT_INITIAL_ANGLE2 + 90, true);
    LOG(LOG_MID, F("cal1 %5d/%5d, %5d/%5d\r\n"), m_steppers[MOT_A1].pos, m_steppers[MOT_A1].tgt_pos,
        m_steppers[MOT_A2].pos, m_steppers[MOT_A2].tgt_pos);
    delay(1000);
    _controlLoop(comm);

#if 1
    LOG(LOG_MID, F("cal2\r\n"));
    delay(1000);
    // Force missing steps on MOTOR1
    for (int cal = 0; cal < 1000; cal++) {
        _step(MOT_A1, 1);
    }

    // GO TO CENTER
    // Force the robot to loose steps and reach the final pos
    configSpeed(MAX_SPEED_M1 / 4, MAX_SPEED_M2 / 10, MAX_SPEED_M3);
    updateSpeedAcc();
    setAxis(ROBOT_INITIAL_ANGLE1, ROBOT_INITIAL_ANGLE2, true);

    LOG(LOG_MID, F("cal3 %5d/%5d, %5d/%5d\r\n"), m_steppers[MOT_A1].pos, m_steppers[MOT_A1].tgt_pos,
        m_steppers[MOT_A2].pos, m_steppers[MOT_A2].tgt_pos);
    delay(1000);
    _controlLoop(comm);

    // MOTOR2
    // Robot doNextGoHome... moving robot to both ends until we loose some steps (brute method because the robot dont have final pos switches
    setAxis(ROBOT_INITIAL_ANGLE1, ROBOT_ABS_MAX_ANGLE2, true);

    LOG(LOG_MID, F("cal4 %5d/%5d, %5d/%5d\r\n"), m_steppers[MOT_A1].pos, m_steppers[MOT_A1].tgt_pos,
        m_steppers[MOT_A2].pos, m_steppers[MOT_A2].tgt_pos);
    delay(1000);    
    _controlLoop(comm);

    LOG(LOG_MID, F("cal5\r\n"));
    delay(1000);
    // Force missing steps on MOTOR2
    for (int cal = 0; cal < 1600; cal++) {
        _step(MOT_A2, 1);
    }

    // Force the robot to its initial centered pos
    configSpeed(MAX_SPEED_M1 / 4, MAX_SPEED_M2 / 4, MAX_SPEED_M3);
    setAxis(ROBOT_INITIAL_ANGLE1, ROBOT_INITIAL_ANGLE2, true);

    LOG(LOG_MID, F("cal6 %5d/%5d, %5d/%5d\r\n"), m_steppers[MOT_A1].pos, m_steppers[MOT_A1].tgt_pos,
        m_steppers[MOT_A2].pos, m_steppers[MOT_A2].tgt_pos);
    delay(1000);    
    _controlLoop(comm);
#endif

    LOG(LOG_MID, F("done!!!\r\n"));

/*
    moveServo1(SERVO1_MIN_PULSEWIDTH);
    delay(600);
    moveServo1(SERVO1_MAX_PULSEWIDTH);
    delay(600);
    moveServo1(SERVO1_NEUTRAL);
    delay(600);
    moveServo2(SERVO2_MIN_PULSEWIDTH);
    delay(600);
    moveServo2(SERVO2_NEUTRAL);
*/    
}
#endif

// Set Robot Axis1:
void RobotArm::setAxis(float angleA1, bool isCheckLimit) {
    if (isCheckLimit) {
        angleA1 = constrain(angleA1, ROBOT_MIN_ANGLE1, ROBOT_MAX_ANGLE1);
    }
    m_steppers[MOT_A1].tgt_pos = angleA1 * M1_AXIS_STEPS_PER_UNIT;
}

// Set Robot Axis2. Axis 2 angle depends on Axis1 angle
void RobotArm::setAxis(float angleA1, float angleA2, bool isCheckLimit) {
    u8 idx;

    if (isCheckLimit) {
        // If angleA1 is not specified (NULL) we take it from robot pos
        if (angleA1 < -199.0) {
            angleA1 = m_tgtAngleA1;
        }

        // If angleA1 is not specified (NULL)we take it from robot pos    
        if (angleA2 < -199.0) { 
            angleA2 = m_tgtAngleA2;
        } 

        angleA1 = constrain(angleA1, ROBOT_MIN_ANGLE1, ROBOT_MAX_ANGLE1);
        idx     = int((angleA1 + ROBOT_MIN_ANGLE1) / -20.0);
        idx     = constrain(idx, 0, (sizeof(TBL_MIN_ANGLE) / sizeof(s16)) - 1);

        // Robot limits... (actually on a fixed table)
        // Calculate the limits of A2 based on A1 angle    
        s16 minA = pgm_read_word(&TBL_MIN_ANGLE[idx]);
        s16 maxA = pgm_read_word(&TBL_MAX_ANGLE[idx]);
        angleA2 = constrain(angleA2, minA, maxA);
        angleA2 = angleA2 + (angleA1 * AXIS2_AXIS1_CORRECTION);
    }
    m_steppers[MOT_A1].tgt_pos = (s32)(angleA1 * M1_AXIS_STEPS_PER_UNIT);
    m_steppers[MOT_A2].tgt_pos = (s32)(angleA2 * M2_AXIS_STEPS_PER_UNIT);

    m_tgtAngleA1 = angleA1;
    m_tgtAngleA2 = angleA2;    
}


// Set Robot axis 3 (height)
void RobotArm::setHeight(float height, bool isCheckLimit) {
    if (isCheckLimit) {
        height = constrain(height, 0, ROBOT_MAX_HEIGHT);
    }
    m_steppers[MOT_Z].tgt_pos = height * M3_AXIS_STEPS_PER_UNIT;
}

void RobotArm::setTarget2Pos(void) {
    for (u8 i = 0; i < 3; i++) {
        m_steppers[i].pos = m_steppers[i].tgt_pos;
        LOG(LOG_MID, F("set %5d/%5d\r\n"), m_steppers[i].pos, m_steppers[i].tgt_pos);
    }
}

// Inverse kinematic formulas for SCARA ROBOT
// This function returns C angle using law of cosines
float RobotArm::_lawOfCosines(float a, float b, float c) {
    return acosf((a * a + b * b - c * c) / (2.0f * a * b));
}

// Euclidean distance between 2 points (0,0 to x,y)
float RobotArm::_distance(float x, float y) {
    return sqrt(x * x + y * y);
}

// time profile: between 520-540us
void RobotArm::inverseKinematic(float x, float y, float len1, float len2, u8 elbow, float *A1, float *A2) {
    float dist;
    float D1, D2;

    if (elbow == 1) {  // inverse elbow solution: reverse X axis, and final angles.
        x = -x;
    }
    dist = _distance(x, y);
    if (dist > (ROBOT_ARM1_LENGTH + ROBOT_ARM2_LENGTH)) {
        dist = (ROBOT_ARM1_LENGTH + ROBOT_ARM2_LENGTH) - 0.001f;
        LOG(LOG_MID, F("IK overflow->limit\r\n"));
    }
    D1   = atan2(y, x);   // 140us
    D2   = _lawOfCosines(dist, len1, len2);   // 175us
    *A1  = (D1 + D2 + ROBOT_AXIS_DEFINITION) * RAD2DEG;
    *A2  = _lawOfCosines(len1, len2, dist) * RAD2DEG - 180;  // 165us
    if (elbow == 1) {
        *A1 = -*A1;
        *A2 = -*A2;
    }
}

// Calculate the total time of a motor movement (trapezoidal profile)
// INPUT: d=distance, v0=initial speed, v1=target max speed, a=acceleration
// OUTUT: total time of the full movement
// There are 3 posible cases:
//  CASE1: Only a deceleration motion is needed (because of the initial v0 speed) The robot will overpass and start a new movement to the final pos
//  CASE2: Only an aceleration/deceleration movement is done (triangular profile) because there are no time to reach the full target speed(v1) [very common]
//  CASE3: Full trapezoidal movement with an acceleration period (until tA), a full speed stage until the deceleration point(tD) where the robot start deceleration until stop.
// tipical timing for a Case2: 85us
float RobotArm::_getMoveTime(float d, float v0, float v1, float a) {
    float   d_D_v0, d_D_v1, d_A_v1, d_A, d_D, d_FS, d_OVERPASS;
    float   t_A, t_D, t_FS, t_F, t_F_2;
    float   vp;
    u8      case_type;

    // calculate the _distance to decelerate until stop at target speed
    // kinematic formula   d = (sqr(v1)-sqr(v0))/(2*a) because v1=0 and acc(a) is negative in deceleration => d = sqr(v1)/2a
    d_D_v1 = (v1 * v1) / (2 * a);

    if (d_D_v1 > d) {
        d_D_v0 = (v0 * v0) / (2 * a);
        // Robot could not reach full speed => case 1 or 2.
        if (d_D_v0 > d) {
            // With the initial speed the robot will overpass => case 1 start deceleration now!            
            case_type = 1;
        } else {
            // The movement will have an aceleration/deceleration (triangular profile).
            case_type = 2;
        }
    } else {
        // kinematic formula   d = (sqr(v1)-sqr(v0))/(2*a)
        d_A_v1 = ((v1 * v1) - (v0 * v0)) / (2 * a);
        // _distance in acceleration ramp to full speed
        // if the total _distance greater than the sum of the _distance in the acceleration and deceleration ramps? => _distance enough for a full speed part?
        if ((d_A_v1 + d_D_v1) < d) {
            // Full trapezoidal profile (we have a full speed part)
            case_type = 3;
        } else {
            // The movement will have an aceleration/deceleration (triangular profile).
            case_type = 2;
        }
    }

    if (case_type == 1) {
        LOG(LOG_MID, F("->C1:\r\n"));

        //CASE 1 not usable right now... => exit without correction...
        return 0; 
        
        t_F        = v0 / a;  // This is really (0-v0)/(-a) because we are decelerating

        // There will be overpass in this case so we need to calculate the new movement!
        d_OVERPASS = d_D_v0 - d;  // This is the new movement that we need to do

        //New conditions:
        d          = d_OVERPASS;
        v0         = 0;

        // To do...
        d_A_v1     = (v1 * v1) / (2 * a);
        d_D_v1     = d_A_v1;
        if ((d_A_v1 + d_D_v1) < d) {
            // Full trapezoidal movement...
            d_FS  = d - (d_A_v1 + d_D_v1);  // Calculate the total _distance that we are at full speed
            t_FS  = d_FS / v1;
            t_A   = v1 / a;
            t_D   = t_A;         // Because we start and end at zero speed
            t_F_2 = t_A + t_FS + t_D;

#ifdef __FEATURE_DEBUG__
            char buf[20];
            catD2str(t_F_2, 6, 3, buf, sizeof(buf), true);
            LOG(LOG_MID, F(" C3:%s\r\n"), buf);
#endif
            t_F += t_F_2;
        } else {
            // Triangular movement...
            vp    = sqrt(a * d);  // simplification because v0=0
            t_A   = vp / a;
            t_D   = t_A;       // Because we start and end at zero speed
            t_F_2 = t_A + t_D;  // total time = time_accelerating + time_decelerating

#ifdef __FEATURE_DEBUG__
            char buf[20];
            catD2str(t_F_2, 6, 3, buf, sizeof(buf), true);            
            LOG((" C2:%s\r\n"), buf);
#endif
            t_F += t_F_2;
        }
    }

    if (case_type == 2) {
        // Basic condition: d = d_A + d_D  (total _distance = distance_acceleration + distance_decelerating
        // vp is the peek velocity (where the movement change from acceleration to deceleration), the point of the triangle
        // d_A = ((vp*vp)-(v0*v0))/2a ; d_D = (vp*vp)/2a ; d = d_A + d_D
        // equation to solve vp: vp = sqrt((2*a*d + v0*v0)/2)
        //vp = quadratic_equation(2,-2*v0,((v0*v0)-(2*d*a)));
        vp  = sqrt((2 * a * d - (v0 * v0)) / 2);
        t_A = mABS(vp - v0) / a;         // always suppose positive values...
        t_D = vp / a;                   // This is really (0-vp)/(-a) because we are decelerating
        t_F = t_A + t_D;                // total time = time_accelerating + time_decelerating

#ifdef __FEATURE_DEBUG__
        char buf[20];
        catD2str(t_A, 6, 3, buf, sizeof(buf), true);
        catD2str(t_D, 6, 3, buf, sizeof(buf));
        LOG(("->C2:%s\r\n"), buf);
#endif
    }
    if (case_type == 3) {
        d_FS = d - (d_A_v1 + d_D_v1);   // Calculate the total _distance that we are at full speed
        t_FS = d_FS / v1;
        t_A  = mABS(v1 - v0) / a;        // always suppose positive values...
        t_D  = v1 / a;                  // This is really (0-v1)/(-a) because we are decelerating
        t_F  = t_A + t_FS + t_D;

#ifdef __FEATURE_DEBUG__
        char buf[20];
        catD2str(t_A, 6, 3, buf, sizeof(buf), true);
        catD2str(t_FS, 6, 3, buf, sizeof(buf));
        catD2str(t_D, 6, 3, buf, sizeof(buf));
        LOG(("->C3:%s\r\n"), buf);
#endif
    }
    return t_F;
}


float RobotArm::_getMoveTime(stepper_t *pStepper) {
    s16     diff;
    s16     pos;
    s16     speed;
    float   t;

    noInterrupts();
    pos   = pStepper->pos;
    speed = pStepper->speed;
    diff  = pStepper->tgt_pos - pos;
    interrupts();

    if (mSign(diff) != mSign(speed)) {
        if (mABS(speed) < (4 * pStepper->tgt_accel)) {  // Check if speed is low enough to suppose speed=0
            speed = 0;                                  // suppose speed=0 and continue with trajectory motor speed adjust
        } else {
            return -1;
        }
    }

    // Target pos and actual speed has the same sign, we are going in the right way...
    diff  = mABS(diff);
    speed = mABS(speed);

    // We are going to resolve the total time of movement
    t = _getMoveTime(diff, speed, pStepper->tgt_speed, pStepper->tgt_accel * 1000);

    return t;
}


// This function adjust the speed of the motors so the movement will be syncronized
// so both motors end at the same time.
// We only run this adjustment if motors are running in the correct direction (if a motor need to invert its movement we dont make the adjust)
// time profile: around 300-350us
void RobotArm::adjustTrajectorySpeed(void) {
    float   t1, t2;
    float   factor;

    LOG(LOG_MID, F("->PLAN\r\n"));
    t1 = _getMoveTime(_getStepper(MOT_A1));
    if (t1 < 0) {
        LOG(LOG_MID, F("minus t1\r\n"));
        return;
    }
    
    t2 = _getMoveTime(_getStepper(MOT_A2));
    if (t2 < 0) {
        LOG(LOG_MID, F("minus t2\r\n"));
        return;
    }

#ifdef __FEATURE_DEBUG__
    char    buf[20];
    catD2str(t1, 6, 3, buf, sizeof(buf), true);
    catD2str(t2, 6, 3, buf, sizeof(buf));
    LOG(LOG_MID, F("t1, t2 : %s\r\n"), buf);
#endif

    if ((t1 == t2) || (t1 < 0.01) || (t2 < 0.01)) {
        LOG(LOG_MID, F("->P Small t!\r\n"));
        return;
    }

    u8          idx;
    stepper_t   *pStepper = _getStepper(idx);;
    
    // Decide wich axis will be slower. That will be the master axis
    if (t1 > t2) {
        // Motor1 is slower so its the master, we adapt motor2...
        idx    = MOT_A2;
        factor = t2 / t1;
    } else {
        // Motor2 is slower so its the master, we adapt motor1...
        idx    = MOT_A1;
        factor = t1 / t2;
    }

    pStepper->tgt_speed = pStepper->tgt_speed * factor;
    pStepper->tgt_accel = pStepper->tgt_accel * factor * factor;
    if (pStepper->tgt_accel < 4) {
        pStepper->tgt_accel = 4;
    }
    
#ifdef __FEATURE_DEBUG__
    catD2str(factor, 6, 3, buf, sizeof(buf), true);
    LOG(LOG_MID, F("f : %s\r\n"), buf);
#endif
}
