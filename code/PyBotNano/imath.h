/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#ifndef _IMATH_H_
#define _IMATH_H_
#include <Arduino.h>
#include "common.h"
#include "config.h"

/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/
#define	DEC_EXP_1		10
#define	DEC_EXP_2		100
#define	DEC_EXP_3		1000
#define	DEC_EXP_4		10000
#define	DEC_EXP_5		100000
#define	DEC_EXP_6		1000000
#define	DEC_EXP_7		10000000

#define PI              3.14159265358979
#define RAD2DEG         57.2957795                          // 180 / pi
#define DEG2RAD         0.01745329251994329576923690768489  // pi  / 180

#define RAD2DEG_EXP_2   5729
#define DEG2RAD_EXP_4   174

/*
*****************************************************************************************
* FUNCTIONS
*****************************************************************************************
*/
u32  isqrt32(u32 n);
s16  d4sin(s16 d1angle);
s16  d4cos(s16 d1angle);
s16  d4acos(s16 d4val);
s16  d4atan2(s16 y, s16 x);

#endif

