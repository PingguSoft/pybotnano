#include "Protocol.h"
#include "Utils.h"

Protocol::Protocol(u8 ucPacketSize) {
    m_stateRx      = STATE_IDLE;
    m_ucOffset     = 0;
    m_ucDataSize   = 0;
    m_ucPacketSize = ucPacketSize;
    m_pRxPackets   = new u8[m_ucPacketSize];
//    m_pTxPackets   = new u8[m_ucPacketSize];
}

Protocol::~Protocol() {
    delete m_pRxPackets;
//    delete m_pTxPackets;
    delete m_pInterface;
}

void Protocol::setInterface(ProtocolInterface *pInterface) {
    m_pInterface = pInterface;
}

void Protocol::setCallback(ProtocolCallback *pCallback) { 
    m_pCallback  = pCallback; 
}

void Protocol::sendStatus(u8 *pData, u8 size) {
/*    
    char buf[10];

    memset(m_pTxPackets, 0, m_ucPacketSize);
    sprintf(m_pTxPackets, "$$%d", status);

    for (u8 i = 0; i < cnt; i++) {
        sprintf(buf, ",%d", *(pData + i));
        strcat(m_pTxPackets, buf);
    }
*/    
    if (m_pInterface) {
        m_pInterface->write(pData, size);
    }
}

static const u8 TBL_CMDS[] PROGMEM = {
    'H',  0,
    'M', 16,
    'I', 16,
    'T', 16,
    'S', 10,
    'C',  0,
    'E',  0,
    'Z',  0,
    'L',  1,
};

void Protocol::processRx(u8 *pBuf, u16 size) {
    u8 j;
    
    for (u16 i = 0; i < size; i++) {
        u8 ch = pBuf[i];

        switch (m_stateRx) {
            case STATE_IDLE:
                if (ch == 'J')
                    m_stateRx = STATE_HEADER_1ST_J;
                break;

            case STATE_HEADER_1ST_J:
                m_stateRx = (ch == 'J') ? STATE_HEADER_2ND_J : STATE_IDLE;
                break;

            case STATE_HEADER_2ND_J:
                m_stateRx = (ch == 'A') ? STATE_HEADER_A : STATE_IDLE;
                break;

            case STATE_HEADER_A:
                for (j = 0; j < sizeof(TBL_CMDS); j += 2) {
                    if (pgm_read_byte(&TBL_CMDS[j]) == ch) {
                        m_ucCmd      = (j >> 1) + 1;
                        m_ucDataSize = pgm_read_byte(&TBL_CMDS[j + 1]);
                        m_ucOffset   = 0;

                        if (m_ucDataSize == 0) {
                            if (m_pCallback) {
                                m_pCallback->onCommand(m_ucCmd, m_pRxPackets, 0);
                            }
                            m_stateRx = STATE_IDLE;
                        } else {
                            m_stateRx = STATE_HEADER_CMD;
                        }
                        break;
                    }
                }
                if (j > sizeof(TBL_CMDS)) {
                    m_ucCmd   = 0;
                    m_stateRx = STATE_IDLE;
                }
                break;

            case STATE_HEADER_CMD:
                if (m_ucOffset < m_ucDataSize) {
                    m_pRxPackets[m_ucOffset++] = ch;
                }
                if (m_ucOffset == m_ucDataSize) {
                    if (m_pCallback) {
                        m_pCallback->onCommand(m_ucCmd, m_pRxPackets, m_ucDataSize);
                    }
                    m_stateRx = STATE_IDLE;
                }
                break;
        }
    }        
}

