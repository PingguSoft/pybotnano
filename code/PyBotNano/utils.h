/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#ifndef _UTILS_H_
#define _UTILS_H_
#include <Arduino.h>
#include <avr/pgmspace.h>
#include "common.h"
#include "config.h"

/*
*****************************************************************************************
* MACROS
*****************************************************************************************
*/
#define BV(bit)             (1 << (bit))
#define mMin(a,b)           ((a) < (b) ?  (a) : (b))
#define mMax(a,b)           ((a) > (b) ?  (a) : (b))
#define mABS(a)             ((a) <  0  ? -(a) : (a))
#define mSign(a)            ((a) <  0  ?  -1  :  1 )
#define mClrMask(x, y)      (x &= ~(y))
#define mSetMask(x, y)      (x |=  (y))
#define mIsMaskSet(x, y)    ((x) & (y))

#define mGet8bitQ(period)   ((period) >> 8)
#define mGet8bitR(period)   ((period) & 0xff)

/*
*****************************************************************************************
* FUNCTIONS
*****************************************************************************************
*/
u32  rand32_r(u32 *seed, u8 update);
u32  rand32();


template <typename T> void PROGMEM_read(const T * sce, T& dest) {
    memcpy_P(&dest, sce, sizeof (T));
}

template <typename T> T PROGMEM_get(const T * sce) {
    static T temp;
    memcpy_P(&temp, sce, sizeof (T));
    return temp;
}

void setLogMask(u16 mask);


char *catD2str(double d, s8 width, u8 precision, char *dest, u16 dest_size, bool clr = FALSE);

#define LOG_HIGH_ONLY   _BV(2)
#define LOG_MID_ONLY    _BV(1)
#define LOG_LOW_ONLY    _BV(0)

#define LOG_NONE        0
#define LOG_HIGH        (LOG_HIGH_ONLY)
#define LOG_MID         (LOG_HIGH | _BV(1))
#define LOG_LOW         (LOG_MID  | _BV(0))


#if __FEATURE_DEBUG__
    void LOG(u16 mask, char *fmt, ... );
    void LOG(u16 mask, const __FlashStringHelper *fmt, ... );
    void DUMP(u16 mask, char *name, u8 *data, u16 cnt);
    #define __PRINT_FUNC__  //LOG(F("%08ld : %s\n"), millis(), __PRETTY_FUNCTION__);
#else
    #define LOG(...)
    #define DUMP(...)
    #define __PRINT_FUNC__
#endif

#endif
