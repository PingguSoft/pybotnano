#include <Arduino.h>
#include <stdarg.h>
#include <math.h>
#include "common.h"
#include "config.h"
#include "utils.h"
#include "Timer.h"

/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/


/*
*****************************************************************************************
* TYPES
*****************************************************************************************
*/
typedef struct {
    u16 period;
    volatile u8  q;  // quotient
    volatile u8  r;  // remainder
} tmr8_ext_t;


/*
*****************************************************************************************
* MACROS
*****************************************************************************************
*/


/*
*****************************************************************************************
* VARIABLES
*****************************************************************************************
*/
static Timer        *m_pThis;

#if defined(__AVR_ATmega328P__)
static tmr8_ext_t   m_tmrExts[2];
static u8           m_tmrHi;
#endif

/*
*****************************************************************************************
* TIMERS
*****************************************************************************************
*/
Timer::Timer(void) {
    m_pThis = this;
    m_pCallback = NULL;
#if defined(__AVR_ATmega328P__)
    m_tmrHi = 0;
    memset(m_tmrExts, 0, sizeof(m_tmrExts));
#endif
}

void Timer::setup(void) {
    TCCR1A = 0;                                 // Timer1 normal mode 0, OCxA,B outputs disconnected
    TCCR1B = BV(CS11);                          // Prescaler=8, => 2Mhz

#if defined(__AVR_ATmega328P__)
    TCCR2A = 0;                                 // Timer2 normal mode 0, OCxA,B outputs disconnected
    TCCR2B = BV(CS21);                          // Prescaler=8, => 2Mhz
#elif defined(__AVR_ATmega32U4__)
    TCCR3A = 0;                                 // Timer1 normal mode 0, OCxA,B outputs disconnected
    TCCR3B = BV(CS31);                          // Prescaler=8, => 2Mhz
#endif
}

#if defined(__AVR_ATmega328P__)
void _tmr2Config(tmr8_ext_t *pTmr, u8 tcnt, volatile u8 *ocr, u8 mask) {
    mClrMask(TIMSK2, mask);                     // disable COMP interrupt

    u16 next = pTmr->period + tcnt;    
    pTmr->q = mGet8bitQ(next);
    pTmr->r = mGet8bitR(next);

    if (pTmr->q == 0 && pTmr->r > 0) {          // too short period ?
        *ocr = mMax(TMR8_MIN_NEXT, pTmr->r);
        mSetMask(TIMSK2, mask);                 // enable COMP interrupt immediately
    } else {
        *ocr = pTmr->r;
    }
}
#endif

void Timer::setTimer(u8 idx, u16 period) {
    u8 mask;
    volatile u8  *ocr8;
    volatile u16 *ocr16;

    if (m_wPeriods[idx] == period)
        return;
    
    switch (idx) {
        // timer-1 16 bit A/B
        case 0:
        case 1:
            if (idx == 0) {
                ocr16 = &OCR1A;
                mask  = BV(OCIE1A);
            } else {
                ocr16 = &OCR1B;
                mask  = BV(OCIE1B);
            }
            
            mClrMask(TIMSK1, mask);             // disable COMP interrupts
            m_wPeriods[idx] = period;
            if (period > 0) {
                *ocr16 = TCNT1 + period;
                mSetMask(TIMSK1, mask);         // enable COMP interrupts
            }
            break;

        case 2:
        case 3:
#if defined(__AVR_ATmega328P__)
            // timer-2 8 bit A/B
            if (idx == 2) {
                ocr8 = &OCR2A;
                mask = BV(OCIE2A);
            } else {
                ocr8 = &OCR2B;
                mask = BV(OCIE2B);
            }

            mClrMask(TIMSK2, BV(TOV2) | mask);  // disable COMP, OVF interrupts
            m_wPeriods[idx] = period;

            if (period > 0) {
                tmr8_ext_t *tmr = &m_tmrExts[idx - 2];
                
                tmr->period = period;
                _tmr2Config(tmr, TCNT2, ocr8, mask);
            }
            mSetMask(TIMSK2, BV(TOV2));         // enable OVF interrupts
            
#elif defined(__AVR_ATmega32U4__)
            // timer-3 16 bit A/B
            if (idx == 2) {
                ocr16 = &OCR3A;
                mask  = BV(OCIE3A);
            } else {
                ocr16 = &OCR3B;
                mask  = BV(OCIE3B);
            }

            mClrMask(TIMSK3, mask);             // disable COMP interrupts
            m_wPeriods[idx] = period;
            
            if (period > 0) {
                *ocr16 = TCNT3 + period;
                mSetMask(TIMSK3, mask);         // enable COMP interrupts
            }
#endif
            break;
    }
}


/*
*****************************************************************************************
* ISR
*****************************************************************************************
*/
inline void _isrTmr16(u8 idx, volatile u16 *tcnt, volatile u16 *ocr, volatile u8 *timsk, u8 mask) {
    u16 nextPeriod = m_pThis->onTimer(idx, *tcnt);

    if (nextPeriod != 0) {
        m_pThis->setPeriod(idx, nextPeriod);
    } else {
        nextPeriod = m_pThis->getPeriod(idx);
    }
    *ocr = *tcnt + nextPeriod;
}

ISR(TIMER1_COMPA_vect) {
    _isrTmr16(0, &TCNT1, &OCR1A, &TIMSK1, BV(OCIE1A));
}

ISR(TIMER1_COMPB_vect) {
    _isrTmr16(1, &TCNT1, &OCR1B, &TIMSK1, BV(OCIE1B));    
}

#if defined(__AVR_ATmega328P__)
inline void _isrTmr8(u8 idx, volatile u8 *ocr, u8 mask) {
    u16 nextPeriod;
    u8  tcnt = TCNT2;

    nextPeriod = m_pThis->onTimer(idx, ((u16)m_tmrHi << 8) + tcnt);
    if (nextPeriod != 0) {
        m_pThis->setPeriod(idx, nextPeriod);
        m_tmrExts[idx - 2].period = nextPeriod;
    }
    _tmr2Config(&m_tmrExts[idx - 2], tcnt, ocr, mask);    
}

ISR(TIMER2_OVF_vect) {
    u8  mask;
    u16 nextPeriod;
    volatile u8 *ocr;

    m_tmrHi++;
    for (u8 i = 0; i < 2; i++) {
        tmr8_ext_t *tmr = &m_tmrExts[i];

        if (tmr->q > 0) {
            tmr->q--;

            if (tmr->q == 0) {
                if (i == 0) {
                    ocr  = &OCR2A;
                    mask = BV(OCIE2A);
                } else {
                    ocr  = &OCR2B;
                    mask = BV(OCIE2B);
                }

                if (tmr->r == 0) {
                    _isrTmr8(2 + i, ocr, mask);
                } else {
                    mSetMask(TIFR2, mask);      // clear  COMP interrupt
                    *ocr = mMax(TMR8_MIN_NEXT, tmr->r);
                    mSetMask(TIMSK2, mask);     // enable COMP interrupt
                }
            }
        }
    }
}

ISR(TIMER2_COMPA_vect) {
    mSetMask(TIFR2, BV(OCIE2A));      // clear  COMP interrupt
    _isrTmr8(2, &OCR2A, BV(OCIE2A));
}

ISR(TIMER2_COMPB_vect) {
    mSetMask(TIFR2, BV(OCIE2B));      // clear  COMP interrupt
    _isrTmr8(3, &OCR2B, BV(OCIE2B));
}

#elif defined(__AVR_ATmega32U4__)

ISR(TIMER3_COMPA_vect) {
    _isrTmr16(2, &TCNT3, &OCR3A, &TIMSK3, BV(OCIE3A));
}

ISR(TIMER3_COMPB_vect) {
    _isrTmr16(3, &TCNT3, &OCR3B, &TIMSK3, BV(OCIE3B));
}


#endif

