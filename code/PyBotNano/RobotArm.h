#ifndef _ROBOTARM_H_
#define _ROBOTARM_H_
#include "common.h"
#include "Timer.h"
#include "ServoDrv.h"
//#include "CommInterface.h"


/*
*****************************************************************************************
* MACROS
*****************************************************************************************
*/
enum {
    MOT_A1 = 0,
    MOT_A2,
    MOT_Z,
};

enum {
    SERVO_WRIST = 0,
    SERVO_GRIPPER
};

typedef struct {
    u8    idx;
    
    u8    pin_step;
    u8    pin_dir;
    u8    pin_endstop;

    volatile u8 *reg_out_step;
    volatile u8 *reg_out_dir;
    volatile u8 *reg_in_endstop;

    u8    mask_step;
    u8    mask_dir;
    u8    mask_endstop;

    volatile s8    dir;
    volatile bool  is_stopped;
    volatile s32   pos;
   
    s32   pos_last;
    
    s16   speed;
    s16   accel;
    bool  is_stopping;

    s16   conf_speed;
    s16   conf_accel;

    u16   max_speed;
    u16   max_degree;

    u16   tmr_period;
    u16   acc_dt;
        
    s32   tgt_pos;
    s16   tgt_speed;
    float tgt_accel;
} stepper_t;

typedef struct {
    s16 angle1;
    s16 angle2;
    s16 pos_z;
} cal_info_t;

/*
*****************************************************************************************
* Class
*****************************************************************************************
*/
class RobotArm : public TimerCallback {
    public:
        RobotArm(void);

        void setup(void);
        void setSteper(u8 idx, u8 pin_step, u8 pin_dir, u8 pin_endstop, s16 max_speed, s16 max_acc);
        void setServo(u8 idx, u8 pin, u16 min, u16 max);
        void enable(void);
        void disable(void);
        
        void configSpeed(s32 speed0, s32 speed1, s32 speed2);
        void configAcceleration(s32 acc0, s32 acc1, s32 acc2);
        void updateSpeedAcc(void);
        void adjustSpeed(void);
        void adjustTrajectorySpeed(void);

        void setAxis(float angleA1, bool isCheckLimit = true);
        void setAxis(float angleA1, float angleA2, bool isCheckLimit = true);
        void setHeight(float height, bool isCheckLimit = true);
        void setTarget2Pos(void);
        s16  getDistance(u8 idx);
        void getStatus(s16 *A1, s16 *A2, s16 *height);
        void inverseKinematic(float x, float y, float len1, float len2, u8 elbow, float *A1, float *A2);

        void moveServo(u8 idx, float percent);
        void writeServo(u8 idx, u16 pwm);


        void startGoHome(void);
        void doNextGoHome(void);
        bool positionControl(u16 dt);
        void dumpMotors(void);
        void testServo(void);

        // virtual functions
        virtual u16 onTimer(u8 idx, u16 cnt);

//      void motorsCalibration(CommInterface *comm);        

    private:
        void  _delay1uS(void);
        void  _step(u8 idx);
        void  _step(u8 idx, s8 dir);

        void  _setSpeed(u8 idx, stepper_t *pStepper, s16 speed, u16 dt, s16 overshoot_comp);
//        void  _controlLoop(CommInterface *comm);
        float _lawOfCosines(float a, float b, float c);
        float _distance(float x, float y);
        float _getMoveTime(float d, float v0, float v1, float a);
        float _getMoveTime(stepper_t *pStepper);
        
        stepper_t *_getStepper(u8 idx)   { return &m_steppers[idx]; }

        Timer        m_timer;
        stepper_t    m_steppers[3];
        float        m_tgtAngleA1;
        float        m_tgtAngleA2;

        bool         m_isCal;
        u8           m_ucCalIdx;

        ServoDrv     m_servo;
};

#endif
